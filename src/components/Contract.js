import React from 'react';
import { Link } from 'react-router-dom';
import Paper from 'material-ui-next/Paper';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui-next/Table';

// TODO delete hardcode
const data = {
  name: 'Frozen yoghurt',
  telephone: 159,
  email: 6.0,
};


export default class Client extends React.Component {
  render() {
    return (
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ФИО</TableCell>
              <TableCell numeric>Телефон</TableCell>
              <TableCell numeric>Email</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell><input type="text" defaultValue={data.name} /></TableCell>
              <TableCell numeric><input type="text" defaultValue={data.telephone} /></TableCell>
              <TableCell numeric><input type="text" defaultValue={data.email} /></TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Link to='/'>Back</Link>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}
