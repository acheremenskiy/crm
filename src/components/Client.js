import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Paper from 'material-ui-next/Paper';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui-next/Table';

// TODO delete hardcode
const data = {
  contracts: [
    {
      id: 1,
      name: 'Авто',
    },
    {
      id: 2,
      name: 'Недвижимость',
    },
    {
      id: 3,
      name: 'Недвижимость',
    },
    {
      id: 4,
      name: 'Авто',
    },
  ],
};


class Client extends React.Component {
  render() {
    const { clients } = this.props;
    const clientData = clients[this.props.match.params.number];
    const contractList = data.contracts.map(contract => (
      <li key={contract.id}>
        <Link to={`/contract/${contract.id}`}>{contract.name}</Link>
      </li>
    ));
    return (
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ФИО</TableCell>
              <TableCell numeric>Телефон</TableCell>
              <TableCell numeric>Email</TableCell>
              <TableCell numeric>Город</TableCell>
              <TableCell numeric>Объекты</TableCell>
              <TableCell numeric>Действия</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell><input type="text" defaultValue={clientData.name} /></TableCell>
              <TableCell numeric><input type="text" defaultValue={clientData.telephone} /></TableCell>
              <TableCell numeric><input type="text" defaultValue={clientData.email} /></TableCell>
              <TableCell numeric><input type="text" defaultValue={clientData.address} /></TableCell>
              <TableCell numeric><ul>{contractList}</ul></TableCell>
              <TableCell numeric><button>Добавить договор</button></TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Link to='/'>Back</Link>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

function mapStateToProps(state) {
  return {
    clients: state.clients.clientById.clients.byId,
  };
}

export default connect(mapStateToProps)(Client);
