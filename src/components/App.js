import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui-next/Table';
import Paper from 'material-ui-next/Paper';
import {
  actions as clientActions,
  selectors as clientSelectors,
} from '../ducks/Clients';
import { selectors as contractSelectors } from '../ducks/Contracts';


class App extends React.Component {
  componentDidMount() {
    this.props.fetchData();
  }

  render() {
    const { allClients, activeContracts } = this.props;
    const clientRow = allClients.map((client) => {
      const activeContractsValue = client.contractIds.reduce((acc, item) => {
        if (activeContracts.includes(item)) {
          return acc + 1;
        }
        return acc;
      }, 0);
      return (
        <TableRow key={client.id}>
          <TableCell><Link to={`client/${client.id}`}>{client.name}</Link></TableCell>
          <TableCell numeric>{client.telephone}</TableCell>
          <TableCell numeric>{client.email}</TableCell>
          <TableCell numeric>{client.address}</TableCell>
          <TableCell numeric>{activeContractsValue}</TableCell>
        </TableRow>
      );
    });

    return (
      <Paper>
        <input type="search" />
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ФИО</TableCell>
              <TableCell numeric>Телефон</TableCell>
              <TableCell numeric>Email</TableCell>
              <TableCell numeric>Город</TableCell>
              <TableCell numeric>Кол-во раб. объектов</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {clientRow}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

function mapStateToProps(state) {
  return {
    allClients: clientSelectors.getAllClients(state.clients),
    activeContracts: contractSelectors.getActiveContracts(state.contracts),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchData: bindActionCreators(clientActions.fetchData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
