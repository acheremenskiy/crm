import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import clients from '../ducks/Clients';
import contracts from '../ducks/Contracts';
import services from '../ducks/Services';

const rootReducer = combineReducers({
  clients,
  contracts,
  services,
  form: formReducer,
});
export default rootReducer;
