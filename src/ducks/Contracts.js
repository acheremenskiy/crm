import { combineReducers } from 'redux';
import { createSelector } from 'reselect';
import { types as clientTypes } from './Clients';


const contractsIdsSelector = state => state.contractsIds;
const contractByIdSelector = state => state.contractById.contracts.byId;
const contractsByClient = state => 1;
const getAllContracts = createSelector(
  contractsIdsSelector,
  contractByIdSelector,
  (ids, byId) => ids.map(id => byId[id]),
);
const getActiveContracts = createSelector(
  getAllContracts,
  contract => contract.filter(item => item.status === 'on').map(item => item.id),
);

export const selectors = { getAllContracts, getActiveContracts };

const initialState = {
  contracts: {
    byId: {
    },
  },
};
const contractById = (state = initialState, action) => {
  switch (action.type) {
    case clientTypes.REQUESTED_FETCH_DATA_SUCCEEDED: {
      const settingContractsState = state;
      for (let i = 0; i < action.clients.length; i += 1) {
        const { contracts } = action.clients[i];
        for (let j = 0; j < contracts.length; j += 1) {
          const serviceIds = contracts[j].services.map(item => item.id);
          const contract = {
            id: contracts[j].id,
            type: contracts[j].type,
            number: contracts[j].number,
            status: contracts[j].status,
            dataOn: contracts[j].dataOn,
            dataOff: contracts[j].dataOff,
            serviceIds,
          };
          settingContractsState.contracts.byId[contracts[j].id] = { ...contract };
        }
      }
      return settingContractsState;
    }
    default:
      return state;
  }
};

const initialIdsState = [];
const contractsIds = (state = initialIdsState, action) => {
  switch (action.type) {
    case clientTypes.REQUESTED_FETCH_DATA_SUCCEEDED: {
      let fetchedContractsIds = [];
      for (let i = 0; i < action.clients.length; i += 1) {
        const contractIds = action.clients[i].contracts.map(item => item.id);
        fetchedContractsIds = fetchedContractsIds.concat(contractIds);
      }
      return fetchedContractsIds;
    }
    default:
      return state;
  }
};


export default combineReducers({
  contractById,
  contractsIds,
});
