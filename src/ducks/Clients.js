import 'regenerator-runtime/runtime';
import { combineReducers } from 'redux';
import { createSelector } from 'reselect';
import { call, put, takeEvery } from 'redux-saga/effects';

export const types = {
  SET_CLIENTS: 'SET_CLIENTS',
  REQUESTED_FETCH_DATA_SUCCEEDED: 'REQUESTED_FETCH_DATA_SUCCEEDED',
  REQUESTED_FETCH_DATA_FAILED: 'REQUESTED_FETCH_DATA_FAILED',
  REQUESTED_FETCH_DATA: 'REQUESTED_FETCH_DATA',
  FETCHED_DATA: 'FETCHED_DATA',
};

export const actions = {
  setClients(clients) {
    return {
      type: types.SET_CLIENTS,
      clients,
    };
  },
  requestFetchData() {
    return {
      type: types.REQUESTED_FETCH_DATA,
    };
  },
  requestFetchDataSuccess(clients) {
    return {
      type: types.REQUESTED_FETCH_DATA_SUCCEEDED,
      clients,
    };
  },
  requestFetchDataError() {
    return {
      type: types.REQUESTED_FETCH_DATA_FAILED,
    };
  },
  fetchData() {
    return {
      type: types.FETCHED_DATA,
    };
  },
};


function* fetchFetchDataAsync() {
  try {
    yield put(actions.requestFetchData());
    const clients = yield call(() => fetch('data.json', { method: 'GET' })
      .then(resp => resp.json()));
    if (!clients) {
      throw new Error('No data');
    }
    yield put(actions.requestFetchDataSuccess(clients));
  } catch (error) {
    yield put(actions.requestFetchDataError());
  }
}

export function* watchedSagas() {
  yield takeEvery(types.FETCHED_DATA, fetchFetchDataAsync);
}

const clientsIdsSelector = state => state.clientsIds;
const clientByIdSelector = state => state.clientById.clients.byId;
const getAllClients = createSelector(
  clientsIdsSelector,
  clientByIdSelector,
  (ids, byId) => ids.map(id => byId[id]),
);

export const selectors = { getAllClients };


const initialState = {
  clients: {
    byId: {
    },
  },
};
const clientById = (state = initialState, action) => {
  switch (action.type) {
    case types.REQUESTED_FETCH_DATA_SUCCEEDED: {
      const settingClientsState = state;
      for (let i = 0; i < action.clients.length; i += 1) {
        const contractIds = action.clients[i].contracts.map(item => item.id);
        const client = {
          id: action.clients[i].id,
          name: action.clients[i].name,
          telephone: action.clients[i].telephone,
          email: action.clients[i].email,
          address: action.clients[i].address,
          note: action.clients[i].note,
          contractIds,
        };
        settingClientsState.clients.byId[action.clients[i].id] = { ...client };
      }
      return settingClientsState;
    }
    default:
      return state;
  }
};

const initialIdsState = [];
const clientsIds = (state = initialIdsState, action) => {
  switch (action.type) {
    case types.REQUESTED_FETCH_DATA_SUCCEEDED:
      return action.clients.map(client => client.id);
    default:
      return state;
  }
};


export default combineReducers({
  clientById,
  clientsIds,
});
