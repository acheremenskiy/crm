import { combineReducers } from 'redux';
import { types as clientTypes } from './Clients';

export const selectors = {
  getAllServices: state => state.servicesIds.map(id => state.serviceById.services.byId[id]),
};

const initialState = {
  services: {
    byId: {
    },
  },
};
const serviceById = (state = initialState, action) => {
  switch (action.type) {
    case clientTypes.REQUESTED_FETCH_DATA_SUCCEEDED: {
      const settingServicesState = state;
      for (let i = 0; i < action.clients.length; i += 1) {
        const { contracts } = action.clients[i];
        for (let j = 0; j < contracts.length; j += 1) {
          const { services } = contracts[j];
          for (let k = 0; k < services.length; k += 1) {
            const service = {
              id: services[k].id,
              name: services[k].name,
              dataOn: services[k].dataOn,
              activity: services[k].activity,
            };
            settingServicesState.services.byId[services[k].id] = { ...service };
          }
        }
      }
      return settingServicesState;
    }
    default:
      return state;
  }
};

const initialIdsState = [];
const servicesIds = (state = initialIdsState, action) => {
  switch (action.type) {
    // case clientTypes.REQUESTED_FETCH_DATA_SUCCEEDED: {
    //   let fetchedServicessIds = [];
    //   for (let i = 0; i < action.clients.length; i += 1) {
    //     const contractIds = action.clients[i].contracts.map(item => item.id);
    //     fetchedServicessIds = fetchedServicessIds.concat(contractIds);
    //   }
    //   return fetchedServicessIds;
    // }
    default:
      return state;
  }
};


export default combineReducers({
  serviceById,
  servicesIds,
});

