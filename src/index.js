import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import App from './components/App';
import Client from './components/Client';
import Contract from './components/Contract';
import Store from './store/Store';

ReactDOM.render(
  <Provider store={Store}>
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={App} />
        <Route path="/client/:number" component={Client} />
        <Route path="/contract/:number" component={Contract} />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('app'),
);

module.hot.accept();
